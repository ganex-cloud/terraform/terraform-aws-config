variable "enabled" {
  description = "(Required) Specifies whether enabled recorder."
  type        = bool
  default     = true
}

variable "name" {
  description = "(Required) Specifies the name of all resources."
  type        = string
}

variable "create_iam_role" {
  description = "(Required) Flag to indicate whether an IAM Role should be created to grant the proper permissions for AWS Config."
  type        = bool
  default     = true
}

variable "global_resource_collector_region" {
  description = "(Required) The region that collects AWS Config data for global resources such as IAM"
  type        = string
}

variable "managed_rules" {
  description = <<-DOC
    A list of AWS Managed Rules that should be enabled on the account.
    See the following for a list of possible rules to enable:
    https://docs.aws.amazon.com/config/latest/developerguide/managed-rules-by-aws-config.html
  DOC
  type = map(object({
    description      = string
    identifier       = string
    input_parameters = any
    tags             = map(string)
    enabled          = bool
  }))
  default = {}
}

variable "create_s3_bucket" {
  description = "(Optional) Automatically create the s3 bucket to cloudtrail."
  type        = bool
  default     = true
}

variable "s3_bucket_name" {
  description = "(Optional) Required if create_s3_bucket is false, the name of S3 bucket."
  type        = string
  default     = null
}

variable "tags" {
  description = "(Optional) A map of tags to assign to the trail"
  type        = map(string)
  default     = {}
}

variable "iam_role_arn" {
  description = <<-DOC
    (Optional) The ARN for an IAM Role AWS Config uses to make read or write requests to the delivery channel and to describe the
    AWS resources associated with the account. This is only used if create_iam_role is false.
    If you want to use an existing IAM Role, set the value of this to the ARN of the existing topic and set
    create_iam_role to false.
    See the AWS Docs for further information:
    http://docs.aws.amazon.com/config/latest/developerguide/iamrole-permissions.html
  DOC
  default     = null
  type        = string
}

variable "force_destroy" {
  type        = bool
  description = "(Required) A boolean that indicates all objects should be deleted from the bucket so that the bucket can be destroyed without error. These objects are not recoverable"
  default     = false
}
