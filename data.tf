data "aws_region" "this" {}
data "aws_caller_identity" "this" {}
data "aws_iam_policy" "aws_config_built_in_role" {
  arn = "arn:aws:iam::aws:policy/service-role/AWS_ConfigRole"
}
