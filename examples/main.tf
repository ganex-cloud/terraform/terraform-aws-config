module "aws-config" {
  source                           = "git::https://gitlab.com/ganex-cloud/terraform/terraform-aws-config.git?ref=master"
  name                             = "test"
  global_resource_collector_region = "us-east-1"
}
