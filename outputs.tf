output "s3_bucket_id" {
  value       = aws_s3_bucket.this[0].id
  description = "Bucket Name (aka ID)"
}

output "s3_bucket_arn" {
  value       = aws_s3_bucket.this[0].arn
  description = "Bucket ARN"
}

output "iam_role" {
  description = <<-DOC
  IAM Role used to make read or write requests to the delivery channel and to describe the AWS resources associated with 
  the account.
  DOC
  value       = var.create_iam_role ? aws_iam_role.this[0].arn : var.iam_role_arn
}
