resource "aws_config_configuration_recorder" "this" {
  name     = var.name
  role_arn = var.create_iam_role ? aws_iam_role.this[0].arn : var.iam_role_arn
  recording_group {
    all_supported                 = true
    include_global_resource_types = local.is_global_recorder_region
  }
}

resource "aws_config_delivery_channel" "this" {
  name           = var.name
  s3_bucket_name = aws_s3_bucket.this[0].id
}

resource "aws_config_configuration_recorder_status" "this" {
  name       = aws_config_configuration_recorder.this.name
  is_enabled = var.enabled
  depends_on = [aws_config_delivery_channel.this]
}

resource "aws_config_config_rule" "rules" {
  for_each   = var.enabled ? var.managed_rules : {}
  depends_on = [aws_config_configuration_recorder_status.this]

  name        = each.key
  description = each.value.description

  source {
    owner             = "AWS"
    source_identifier = each.value.identifier
  }

  input_parameters = length(each.value.input_parameters) > 0 ? jsonencode(each.value.input_parameters) : null
  tags             = var.tags
}
