locals {
  is_global_recorder_region = var.global_resource_collector_region == data.aws_region.this.name
}
