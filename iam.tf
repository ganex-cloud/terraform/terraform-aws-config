resource "aws_iam_role" "this" {
  count              = var.create_iam_role ? 1 : 0
  assume_role_policy = data.aws_iam_policy_document.assume[0].json
  name               = "Config-${var.name}"
}

data "aws_iam_policy_document" "assume" {
  count = var.enabled && var.create_iam_role ? 1 : 0

  statement {
    actions = [
      "sts:AssumeRole",
    ]

    principals {
      identifiers = [
        "config.amazonaws.com",
      ]

      type = "Service"
    }
  }
}

resource "aws_iam_role_policy_attachment" "config_policy" {
  count      = var.create_iam_role ? 1 : 0
  role       = aws_iam_role.this[0].name
  policy_arn = data.aws_iam_policy.aws_config_built_in_role.arn
}
